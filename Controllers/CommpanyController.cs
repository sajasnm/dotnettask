using Microsoft.AspNetCore.Mvc;
using dotnettask.Models;
// using dotnettask.Services;

namespace dotnettask.CommpanyController
{

    [ApiController]
    [Route("api/[controller]")]
    public class CommpanyController : ControllerBase
    {



        private readonly ICommpanyService _commpanyService;

        public CommpanyController(ICommpanyService commpanyService)
        {
            _commpanyService = commpanyService;

        }

        [HttpGet("employees/{page}")]
        //[Route("GetAll")]
        public async Task<ActionResult<ServiceResponse<List<GetEmployeeDto>>>> GetEmployee(int page)
        {


            return Ok(await _commpanyService.GetAllEmployees(page));
        }

        [HttpGet("department")]
        //[Route("GetAll")]
        public async Task<ActionResult<ServiceResponse<List<GetDeparmentDto>>>> GetDepartment()
        {


            return Ok(await _commpanyService.GetAllDepartment());
        }


        [HttpGet("employee/{id}")]
        public async Task<ActionResult<ServiceResponse<GetEmployeeDto>>> GetById(int id)
        {
            return Ok(await _commpanyService.GetEmplyeeById(id));
        }

        [HttpPost("employee")]
        public async Task<ActionResult<ServiceResponse<List<GetEmployeeDto>>>> CreateEmployee(CreateEmployeeDto newEmployee)
        {
            return Ok(await _commpanyService.CreateEmployee(newEmployee));
        }

        [HttpPost("department")]
        public async Task<ActionResult<ServiceResponse<List<GetEmployeeDto>>>> CreateDepartment(CreateDeparmentDto newDepartment)
        {
            return Ok(await _commpanyService.CreateDepartment(newDepartment));
        }


        [HttpPut("employee")]
        public async Task<ActionResult<ServiceResponse<List<GetEmployeeDto>>>> UpdateEmployee(UpdateEmployeeDto updateEmployee)
        {
            var response = await _commpanyService.UpdateEmployee(updateEmployee);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("employee/{id}")]
        public async Task<ActionResult<ServiceResponse<List<GetEmployeeDto>>>> DeleteEmployee(int id)
        {

            var response = await _commpanyService.DeleteEmployee(id);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}