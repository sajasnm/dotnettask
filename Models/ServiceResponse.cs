namespace dotnettask.Models
{
    public class ServiceResponse<T>
    {
        public T Data { get; set; }
        public bool Success { get; set; } = true;
        public string Message { get; set; } = null;
        public int CurrentPage { get; set; } = 0;
        public int Pages { get; set; } = 0;

    }
}