using System.Text.Json.Serialization;

namespace dotnettask
{
    //Character
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string Gender { get; set; } = string.Empty;

         [JsonIgnore]
        public Department Department { get; set; }
        public int DepartmentId { get; set; }

    }
}