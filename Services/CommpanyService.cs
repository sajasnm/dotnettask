
namespace dotnettask
{
    public class CommpanyService : ICommpanyService
    {

        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public CommpanyService(DataContext dataContext, IMapper mapper)
        {
            _context = dataContext;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<List<GetDeparmentDto>>> CreateDepartment(CreateDeparmentDto newDepartment)
        {
            var serviceResponse = new ServiceResponse<List<GetDeparmentDto>>();
            Department department = _mapper.Map<Department>(newDepartment);
            _context.Departments.Add(department);
            await _context.SaveChangesAsync();
            var departments = await _context.Departments.ToListAsync();
            serviceResponse.Data = departments.Select(c => _mapper.Map<GetDeparmentDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetEmployeeDto>>> CreateEmployee(CreateEmployeeDto newEmployee)
        {
            var serviceResponse = new ServiceResponse<List<GetEmployeeDto>>();
            Employee employee = _mapper.Map<Employee>(newEmployee);
            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();
            var employees = await _context.Employees.Include(e => e.Department).ToListAsync();
            serviceResponse.Data = employees.Select(c => _mapper.Map<GetEmployeeDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetEmployeeDto>>> DeleteEmployee(int id)
        {
            var serviceResponse = new ServiceResponse<List<GetEmployeeDto>>();
            try
            {
                var employees = await _context.Employees.Include(c => c.Department).ToListAsync();

                Employee employee = await _context.Employees.Where(c => c.Id == id).FirstAsync();

                _context.Employees.Remove(employee);
                await _context.SaveChangesAsync();


                serviceResponse.Data = employees.Select(c => _mapper.Map<GetEmployeeDto>(c)).ToList();

            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;

            }

            return serviceResponse;

        }

        public async Task<ServiceResponse<List<GetDeparmentDto>>> GetAllDepartment()
        {
            var serviceResponse = new ServiceResponse<List<GetDeparmentDto>>();
            var departments = await _context.Departments.Include(c => c.Employees).ToListAsync();
            serviceResponse.Data = departments.Select(c => _mapper.Map<GetDeparmentDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetEmployeeDto>>> GetAllEmployees(int page)
        {
            var serviceResponse = new ServiceResponse<List<GetEmployeeDto>>();
            // var employees = await _context.Employees.Include(c => c.Department).ToListAsync();

            var pageResult = 3f;
            var pageCount = Math.Ceiling(_context.Employees.Count() / pageResult);
            var employees = await _context.Employees.Include(c => c.Department).Skip((page - 1) * (int)pageResult).Take((int)pageResult).ToListAsync();
            serviceResponse.Data = employees.Select(c => _mapper.Map<GetEmployeeDto>(c)).ToList();
            serviceResponse.CurrentPage = page;
            serviceResponse.Pages = (int)pageCount;
            // serviceResponse.Data = employees.Select(c => _mapper.Map<GetEmployeeDto>(c)).ToList();
            return serviceResponse;

        }

        public async Task<ServiceResponse<GetEmployeeDto>> GetEmplyeeById(int id)
        {
            var serviceResponse = new ServiceResponse<GetEmployeeDto>();
            serviceResponse.Data = _mapper.Map<GetEmployeeDto>(await _context.Employees.Where(c => c.Id == id).Include(c => c.Department).FirstAsync());
            return serviceResponse;


        }

        public async Task<ServiceResponse<GetEmployeeDto>> UpdateEmployee(UpdateEmployeeDto updateCharacter)
        {


            var serviceResponse = new ServiceResponse<GetEmployeeDto>();
            try
            {

                var dbEmployee = await _context.Employees.FindAsync(updateCharacter.Id);
                // var hero = heroes.Find(h => h.Id == request.Id);

                // var hero=heroes[id];
                // if (dbHero == null)
                //     return BadRequest("Hero not found.");
                // heroes.Add(hero);
                dbEmployee.Name = updateCharacter.Name;
                dbEmployee.Address = updateCharacter.Address;
                // dbHero.Department = request.LastName;
                // dbHero.Place = request.Place;

                await _context.SaveChangesAsync();
                // Employee employee = await _context.Employees.Where(c => c.Id == updateCharacter.Id).FirstAsync();

                // _context.Employees.Update(employee);
                // await _context.SaveChangesAsync();


                // Character character = characters.FirstOrDefault(c => c.Id == updateCharacter.Id);

                // character.Name = updateCharacter.Name;
                // character.HitPoints = updateCharacter.HitPoints;
                // character.Strength = updateCharacter.Strength;
                // character.Defence = updateCharacter.Defence;
                // character.Inteligence = updateCharacter.Inteligence;
                // character.Class = updateCharacter.Class;
                serviceResponse.Data = _mapper.Map<GetEmployeeDto>(dbEmployee);

            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;

            }

            return serviceResponse;

        }
    }
}




// using Microsoft.AspNetCore.Mvc;
// using dotnettask.Models;

// namespace dotnettask;

// [ApiController]
// [Route("[controller]")]
// public class CharacterController : ControllerBase
// {



//     private readonly DataContext _context;
//     public CharacterController(DataContext dataContext)
//     {
//         _context = dataContext;
//     }

//     [HttpGet]

//     //ServiceResponse
//     public async Task<ServiceResponse<ActionResult<List<GetEmployeeDto>>>> Get(int userId)
//     {
//         var characters = await _context.Characters.Where(c => c.UserId == userId).Include(c => c.Weapon).Include(c => c.Skils).ToListAsync();
//         return characters;

//     }

//     [HttpPost]
//     public async Task<ActionResult<List<GetEmployeeDto>>> Create(CreateCharacterDto req)
//     {

//         var user = await _context.Users.FindAsync(req.UserId);

//         if (user == null)
//         {
//             return NotFound();
//         }
//         var newCharacter = new Character
//         {
//             Name = req.Name,

//             RpgClass = req.RpgClass,

//             User = user
//         };

//         _context.Characters.Add(newCharacter);
//         await _context.SaveChangesAsync();
//         return await Get(newCharacter.UserId);
//     }


//     [HttpPost("weapon")]
//     public async Task<ActionResult<GetEmployeeDto>> CreateWeapon(CreateWeaponDto req)
//     {

//         var character = await _context.Characters.FindAsync(req.CharacterId);

//         if (character == null)
//         {
//             return NotFound();
//         }
//         var newWeapon = new Weapon
//         {
//             Name = req.Name,

//             Damage = req.Damage,

//             Character = character
//         };

//         _context.Weapons.Add(newWeapon);
//         await _context.SaveChangesAsync();
//         return character;
//     }

//     [HttpPost("skill")]
//     public async Task<ActionResult<GetEmployeeDto>> AddCharecterSkil(CreateCharecterSkillDto req)
//     {

//         var character = await _context.Characters.Where(c => c.Id == req.CharacterId).Include(c => c.Skils).FirstOrDefaultAsync();

//         if (character == null)
//         {
//             return NotFound();
//         }

//         var skill = await _context.Skils.FindAsync(req.SkillId);

//         if (skill == null)
//         {
//             return NotFound();
//         }
//         character.Skils.Add(skill);

//         await _context.SaveChangesAsync();
//         return character;
//     }
// }