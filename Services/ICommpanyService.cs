
namespace dotnettask
{
    public interface ICommpanyService
    {
        Task<ServiceResponse<List<GetEmployeeDto>>> GetAllEmployees(int page);
        Task<ServiceResponse<GetEmployeeDto>> GetEmplyeeById(int id);
        Task<ServiceResponse<List<GetEmployeeDto>>> CreateEmployee(CreateEmployeeDto newEmployee);
        Task<ServiceResponse<GetEmployeeDto>> UpdateEmployee(UpdateEmployeeDto updateEmployee);
        Task<ServiceResponse<List<GetEmployeeDto>>> DeleteEmployee(int id);
        Task<ServiceResponse<List<GetDeparmentDto>>> CreateDepartment(CreateDeparmentDto newDepartment);
        Task<ServiceResponse<List<GetDeparmentDto>>> GetAllDepartment();
    }
}