
namespace dotnettask
{
    public class GetEmployeeDto
    {
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string Gender { get; set; } = string.Empty;
        public Department Department { get; set; }

    }
}