
namespace dotnettask
{
    public class CreateEmployeeDto
    {
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string Gender { get; set; } = string.Empty;
        public int DepartmentId { get; set; }

    }
}