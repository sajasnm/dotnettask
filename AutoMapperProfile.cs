
namespace dotnettask
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Employee, GetEmployeeDto>();
            CreateMap<Department, GetDeparmentDto>();
            CreateMap<CreateEmployeeDto, Employee>();
            CreateMap<CreateDeparmentDto, Department>();
        }
    }
}